# tap-faster

This is a mobile android game that is used to test your reaction. 
It was made for a final project and is very basic.

## Installation

1. Clone the repository

2. Open the project file in android studio

3. Ensure you have an emulator setup

4. Click the run button at the top right


## License

I chose the MIT license because it seems to be pretty common and it's simple and permissive.
It let's people use it commercially, modify it etc... I've used it for other projects.
